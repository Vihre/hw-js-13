let listImg = document.querySelectorAll('.image-to-show');
let firstImg = listImg[0];
let lastImg = listImg[listImg.length - 1];
let stopBtn = document.querySelector('.stop');
let showBtn = document.querySelector('.show');

let slider = () => {
	const currentImg = document.querySelector('.active');

	if (currentImg !== lastImg) {
		currentImg.classList.remove('active');
		currentImg.nextElementSibling.classList.add('active');
	} else {
		currentImg.classList.remove('active');
		firstImg.classList.add('active');
	};
};

let timer = setInterval(slider, 3000);

stopBtn.addEventListener('click', () => {
	document.querySelector('.active').classList.remove('fade-in-out')
	clearInterval(timer);
	showBtn.disabled = false;
	stopBtn.disabled = true;
});

showBtn.addEventListener('click', () => {
	listImg.forEach(item => {
		if (!item.classList.contains('fade-in-out')) {
			item.classList.add('fade-in-out')
		}
	})
	timer = setInterval(slider, 3000);
	showBtn.disabled = true;
	stopBtn.disabled = false;
});
